/*
HOW TO CONFIGURE:
-           n: number of region
- paletteName: Palett of brewer coloro to use
-        conn: global variable used as input

*/


// ================= ================= ================= ================= =================
/*!!!!CHANGE ONLY HERE!!!!*/
var title0="dataset 0";
var conns0 = [
    { groups: "B", value : 35},
    { groups: "A,B", value : 17},
    { groups: "B,C", value : 6},
    { groups: "A,B,C", value : 9},
    { groups: "C", value : 23},
    { groups: "A", value : 21},
    { groups: "D", value : 21},
    { groups: "D,A", value : 21}

 ];
 var info0="";

 // devono comparire nello stesso ordine in cui sono definiti nell connection
 var legend0 = [
    {label: "data B", code : "B"},
	{label: "data A", code : "A"},
	{label: "data C", code : "C"},
	{label: "data D", code : "D"}
 ];
// ---------------------------------------
var title1="dataset 1";
 var conns1 = [
    { groups: "B", value : 35},
    { groups: "A,B", value : 17},
    { groups: "B,C", value : 6},
    { groups: "C", value : 23},
    { groups: "A", value : 21}
 ];
var info1="";
var legend1 = [
    {label: "data B", code : "B"},
    {label: "data A", code : "A"},
    {label: "data C", code : "C"}
 ];
// --------------------------------------- 
 var title2="dataset 2";
 var conns2 = [
    { groups: "B", value : 35},
    { groups: "A", value : 21},
    { groups: "B,A", value : 4}

 ];
var info2="";
var legend2 = [
    {label: "data B", code : "B"},
    {label: "data A", code : "A"}
 ];
// --------------------------------------- 
 var title3="dataset 3";
  var conns3 = [
    { groups: "B", value : 8000},
    { groups: "A", value : 2100},
    { groups: "C", value : 3500},
    { groups: "D", value : 2321},
    { groups: "E", value : 3345},
    { groups: "F", value : 2123},
    { groups: "B,A", value : 423}

 ];
 
 var info3="";
 var legend3 = [
    {label: "data B", code : "B"},
    {label: "data A", code : "A"},
    {label: "data C", code : "C"},
    {label: "data D", code : "D"},
    {label: "data E", code : "E"},
    {label: "data E", code : "E"}
 ];
 

var maxDataset = 4; // UPDATE IT
// ================= ================= ================= ================= =================

var conns = conns0;
var paletteName = "YlOrRd";
var n = calculateUniqueGroup (conns);
var selectedDataset = 0;

// ================= ================= ================= ================= =================

function dumpDataset(sD)
{
	var dumpConns = eval('conns' + sD);	
	var dumpText;
	dumpText="<h3>"+eval('title' + sD)+"</h3>"
	dumpText=dumpText+"<ul>";
	for ( c in dumpConns )
	{
		dumpText=dumpText+"<li>";
		dumpText=dumpText+"( "+dumpConns[c].groups.replace(/,/g, " - ")+" )";
		dumpText=dumpText+" : " +dumpConns[c].value;		
		dumpText=dumpText+"</li>"
	}
	dumpText=dumpText+"</ul>";
	return dumpText;
}

// ================= ================= ================= ================= =================
function calculateDataset(sD){
    selectedDataset = sD;
	conns = eval('conns' + selectedDataset);
	n = calculateUniqueGroup (conns);
}

// ================= ================= ================= ================= =================
function calculateUniqueGroup (con)
{
	
	var l = con.length
	var maxGroupsLength = 0;
    for (var i=0; i<l; i++) {
      var conn_groups = con[i].groups;
      var conn_groups_splitted = conn_groups.split(',');
      var ls = conn_groups_splitted.length;
		if ( maxGroupsLength < ls ){
			maxGroupsLength = ls;
		}
     }

     return maxGroupsLength+1;

};
