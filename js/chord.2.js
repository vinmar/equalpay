// From http://mkweb.bcgsc.ca/circos/guide/tables/
var chord;
var svg;


function ShowTooltip(evt)
{
  tooltip.setAttributeNS(null, "x",evt.clientX-8);
  tooltip.setAttributeNS(null, "y",evt.clientY-5);
  tooltip.setAttributeNS(null, "visibility","visible");
}

function HideTooltip()
{
  tooltip.setAttributeNS(null,"visibility","hidden");
}



function draw(){
$("#chart").empty();

chord = d3.layout.chord()
  .padding(.05)
  //.sortSubgroups(d3.descending)
  //.sortSubgroups(d3.ascending)
  .matrix([
    [ 21, 9, 8],
    [ 9, 35, 6],
    [ 8, 6, 23]
  ])
  .connections(conns);
  ;

var chordColor;
//set #groups +1
chordColor = generateBColors(n+1, "YlOrRd");


var w = 600,
    h = 600,
    r0 = Math.min(w, h) * .41, //.41 fixed value
    r1 = r0 * 1.1; //1.1 fixed value

var fill = d3.scale.ordinal()
    .domain(d3.range(n))
    .range(chordColor);

svg = d3.select("#chart")
  .append("svg")
    .attr("width", w)
    .attr("height", h)
    .attr("onload","init(evt)")
  .append("g")
    .attr("transform", "translate(" + w / 2 + "," + h / 2 + ")");

var arc_width = r1-r0;	



svg.append("defs")
  .selectAll("path")
    .data(chord.groups) // data insertion
    .enter().append("path")
    .attr("d", d3.svg.arc().innerRadius(r0).outerRadius(r1-arc_width))
  .attr("id", function(d) { return ("path"+d.index); });

svg.append("text")
  .attr("x", 0)
  .attr("y", 0)
  .attr("dy", -30)
  .attr("dx", 0)
  .attr("textLenght", 0)
  .style("fill", "#000")
  .style("font-size", 16 )
  .selectAll("textPath")
    .data(chord.groups) // data insertion
    .enter().append("textPath")
  .attr("xlink:href", function(d) { return ("#path"+d.index); })
  .text( function(d) { if ( eval("legend"+selectedDataset)[d.index] ) return eval("legend"+selectedDataset)[d.index].label; } );

svg.append("g")
  .selectAll("path")
    .data(chord.groups) // data insertion
    .enter().append("path")
    .style("fill", function(d) { return fill(d.index); })
    .style("stroke", function(d) { return fill(d.index); })
    .attr("d", d3.svg.arc().innerRadius(r0).outerRadius(r1))
      .on("mouseover", fade(.1))
  .on("mouseout", fade(1));
     


	
var ticks = svg.append("g")
  .selectAll("g")
    .data(chord.groups)
  .enter().append("g")
  .selectAll("g")
    .data(groupTicks)
  .enter().append("g")
    .attr("transform", function(d) {
      return "rotate(" + (d.angle * 180 / Math.PI - 90) + ")"
          + "translate(" + r1 + ",0)";		  
    });

ticks.append("line")
    .attr("x1", 1)
    .attr("y1", 0)
    .attr("x2", 5)
    .attr("y2", 0)
    .style("stroke", "#000");

ticks.append("text")
    .attr("x", 8)
    .attr("dy", ".35em")
    .attr("text-anchor", function(d) {
      return d.angle > Math.PI ? "end" : null;
    })
    .attr("transform", function(d) {
      return d.angle > Math.PI ? "rotate(180)translate(-16)" : null;
    })
    .text(function(d) { return d.label ; });

svg.append("g")
  .attr("class", "chord")
  .selectAll("path")
  .data(chord.chords)
  .enter().append("path")
  .style("fill", function(d) { return fill(); })  // hack it!!!
  .attr("d", d3.svg.chord().radius(r0))
  .attr("onmousemove", "ShowTooltip(evt)")
  .attr("onmouseout", "HideTooltip()")
  .style("opacity", 1);



svg.append("text")
  .attr("class", "tooltip")
  .attr("id", "tooltip")
  .attr("x",0)
  .attr("y",0)
  .attr("visibility","hidden")
  .text("ciao");

 
}
/** Returns an array of tick angles and labels, given a group. */
function groupTicks(d) {
  var k = (d.endAngle - d.startAngle) / d.value;
  return d3.range(0, d.value, 1000).map(function(v, i) {  
    var tmplabel;
    if ( i != 0){
		  tmplabel = i % 5 ? null : v / 1000 + "k";		
    }
    return {
      angle: v * k + d.startAngle,
	    label : tmplabel
    };
  });
}

/** Returns an event handler for fading a given chord group. */
function fade(opacity) {
  return function(g, i) {
    svg.selectAll("g.chord path")
        .filter(function(d) {
          return d.source.index != i && d.target.index != i;
        })
      .transition()
        .style("opacity", opacity);
  };
}



function init(evt)
  {
    alert("c");
    if ( window.svgDocument == null )
    {
      svgDocument = evt.target.ownerDocument;
    }
    tooltip = svgDocument.getElementById('tooltip');
  }


